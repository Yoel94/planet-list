import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Planet } from '../interfaces';
import { DataService } from '../data.service';


@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.scss']
})
export class PlanetDetailComponent implements OnInit {

  @Input() planet: {next: string, results: Planet[]}

  loading = true;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private location: Location,
  ) { };

  ngOnInit(): void {
    this.getPlanet();
  }

  getPlanet(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dataService.getPlanet(id)
    .subscribe(planet => {this.planet = planet; this.loading = false})
  };

  goBack(): void {
    this.location.back();
  }

};
