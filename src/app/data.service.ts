import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/expand';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/catch';
import { Planet } from './interfaces';

@Injectable()
export class DataService {

  proxyurl = "https://cors-anywhere.herokuapp.com/";
  url = 'https://swapi.co/api/planets/';


  constructor(
    private http: HttpClient
  ) { };


  getPlanet(id: number): Observable<{next: string, results: Planet[]}> {
    const url = `${this.url}${id}`;
    return this.http.get<{next: string, results: Planet[]}>(url);
  };

  getPlanets() : Observable<{next: string, results: Planet[]}> {
    
    return Observable.create(observer => {
      this.getPage("http://swapi.co/api/planets/")
        .expand((data) => {
            return data.next ? this.getPage(data.next) : Observable.empty();
        })
        .reduce((acc, data: any) => {
            return acc.concat(data.results);
          }, [])
        .catch(error => observer.error(error))
        .subscribe((planets) => {
              observer.next(planets);
              observer.complete();
        });
});
};

  private getPage(url: string): Observable<{next: string, results: Planet[]}> {
  return this.http.get(this.proxyurl + url)
          .map(response => {
                  let body: any = response;
                  return {
                    next: body.next,
                    results: body.results as Planet[]
                  };
              }
          );
            }
}
