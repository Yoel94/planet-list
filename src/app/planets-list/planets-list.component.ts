import { Component, OnInit, HostListener, Input, Inject } from '@angular/core'
import { DataService } from '../data.service'
import { Planet} from '../interfaces'
import { startsWithPipe } from '../filter.pipe'
import { trigger, state, transition, style, animate } from '@angular/animations'
import { DOCUMENT } from '@angular/common'

@Component({
  selector: 'app-planets-list',
  templateUrl: './planets-list.component.html',
  styleUrls: ['./planets-list.component.scss'],
  animations:[ 
    trigger('fade',
    [ 
      state('void', style({ opacity : 0})),
      transition(':enter',[ animate(300)]),
      transition(':leave',[ animate(500)]),
    ]
)]
})
export class PlanetsListComponent implements OnInit {

  @Input() planets: {next: string, results: Planet[]}

  searchText: string = ''
  index: number = 10
  loading = true

  constructor(
    private dataService: DataService,
    @Inject(DOCUMENT) document
  ) { }

  ngOnInit() {

    this.dataService.getPlanets()
    .subscribe(data => {this.planets = data; this.loading = false})
  }

  filterNumber(n) {
    this.index = n
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
     if (window.pageYOffset > 550) {
       let element = document.getElementById('searchbar')
       element.classList.add('sticky')
     } else {
      let element = document.getElementById('searchbar')
        element.classList.remove('sticky')
     }

  }

}
